function [emoVal, spkrVal, phrsVal] = getattr(filename, database)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  get attributes of wav files from EMOVO corpus
%
%   emotions (emo):             speakers (spkrs):    phrases(phrs):
%   code name - eng name - ita
%   0    anger      rabbia      7    male 1          13 b1   20 l3
%   1    disgust    disgusto    8    male 2          14 b2   21 l4
%   2    fear       paura       9    male 3          15 b3   22 n1
%   3    joy        giola       10   female 1        16 d1   23 n2
%   4    neural     neurale     11   female 2        17 d2   24 n3
%   5    sadness    tristezza   12   female 3        18 l1   25 n4
%   6    surprise   sorpresa                         19 l2   26 n5
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  get attributes of wav files from RAVDESS DATABASE
%
%   emotions (emo):     speakers (spkrs):           phrases(phrs):
%   0 (5)   anger       7   male 1 ; 19  female 1   31  phrase 1
%   1 (7)   disgust     8   male 2 ; 20  female 2   32  phrase 2
%   2 (6)   fear       	9   male 3 ; 21  female 3
%   3 (3)   happy       10  male 4 ; 22  female 4
%   4 (1)   neural     	11  male 5 ; 23  female 5
%   5 (4)   sadness    	12  male 6 ; 24  female 6
%   6 (8)   surprise    13  male 7 ; 25  female 7
%                       14  male 8 ; 26  female 8
%                       15  male 9 ; 27  female 9
%                       16  male 10; 28  female 10
%                       17  male 11; 29  female 11
%                       18  male 12; 30  female 12
%
% [ phrase 1: "Kids are talking by the door" ]
% [ phrase 2: "Dogs are sitting by the door" ]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  get attributes of wav files from GERMAN DATABASE
%
%   emotions (emo):     speakers (spkrs):   phrases(phrs):
%   0   anger           7   male 1          17  phrase 1
%   1	joy             8   male 2          18  phrase 2
%   2	sadness       	9   male 3          19
%   3   fear            10  male 4          20
%   4	disgust     	11  male 5          21
%   5   boredom         12  female 1        22
%   6	neural          13  female 2        23
%                       14  female 3        24
%                       15  female 4        25
%                       16  female 5        26
%                                           27

switch database
    case 'emovo/'
        emotoinsTMP = { 'rab', 'dis', 'pau', 'gio', 'neu', 'tri', 'sor' };
        speakersTMP = { 'm1', 'm2', 'm3', 'f1', 'f2', 'f3'};
        phrasesTMP  = { 'b1', 'b2', 'b3', 'd1', 'd2', 'l1', 'l2', 'l3' ...
                        'l4', 'n1', 'n2', 'n3', 'n4', 'n5' };
        
        emotionsCode = [ 0, 1, 2, 3, 4, 5, 6 ];
        speakersCode = [ 7, 8, 9, 10, 11, 12 ];
        phrasesCode  = [ 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 ...
                         25, 26 ];
        
        emo  = filename(1:3);
        spkr = filename(5:6);
        phrs = filename(8:9);
        
        for i=1:length(emotoinsTMP)
            if(emo == emotoinsTMP{i})
                emoVal = emotionsCode(i);
            end
        end
        
        for i=1:length(speakersTMP)
            if(spkr == speakersTMP{i})
                spkrVal = speakersCode(i);
            end
        end
        
        for i=1:length(phrasesTMP)
            if(phrs == phrasesTMP{i})
                phrsVal = phrasesCode(i);
            end
        end
    case 'ravdess/'
        % some do
    case 'german/'
        % some do
    otherwise
        disp('\nERROR: <- WRONG LABEL OF DATABASE! ->')
        return
end
end