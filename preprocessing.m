function [x_mono, fs_new] = preprocessing(filename)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  transcoding of input audiofiles:
%   sample rate - 32000 Hz
%   channels    - mono (average between the 1st and the 2nd channels)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fs_new = 32e3;
[x,fs] = audioread(filename);
x_mono = sum(x,2) / size(x,2);

[p, q] = rat(fs_new / fs);
x_mono = resample(x_mono, p, q);
sizeSig = size(x_mono);

fprintf('\n\nsignal pre-processing has been successfully completed.');
fprintf('\nnew fs is %i, number of channels: %i.\n', fs_new, sizeSig(1));

end