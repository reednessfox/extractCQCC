function data = getalldataCQCC(features, emo, spkr, phrs)

attr = [];
tmpEmo  = [];
tmpSpkr = [];
tmpPhrs = [];

sz = size(features);

for k=1:sz(2)
    tmpEmo  = [tmpEmo, emo];
    tmpSpkr = [tmpSpkr, spkr];
    tmpPhrs = [tmpPhrs, phrs];
end
        
attr = cat(2, attr, tmpEmo');
attr = cat(2, attr, tmpSpkr');
attr = cat(2, attr, tmpPhrs');
data = cat(2, features', attr);

end