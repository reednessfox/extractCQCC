% This is a part of small university project (course work #2) on the
% recognition of the emotions of a speaker based on the CQCC & MFCC.
%
% Code based on the works CQCC V2.0 FEATURES.
% Matlab implementation of constant Q cepstral coefficients successfully
% used for:
%  - spoofed speech detection (see CQCC V1.0);
%  - automatic speaker verification.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Copyright (C) 2016 EURECOM, France.
%
% This work is licensed under the Creative Commons
% Attribution-NonCommercial-ShareAlike 4.0 International
% License. To view a copy of this license, visit
% http://creativecommons.org/licenses/by-nc-sa/4.0/
% or send a letter to
% Creative Commons, 444 Castro Street, Suite 900,
% Mountain View, California, 94041, USA.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% For further details, refer to the following publication:
% Todisco, M., Delgado, H., Evans, N., Articulation Rate Filtering of
% CQCC Features for Automatic Speaker Verification. Proceeding of
% INTERSPEECH: 17th Annual Conference of the International Speech
% Communication Association, September 8-13, 2016, San Francisco, USA.
%
% Todisco, M., Delgado, H., Evans, N., A new feature for automatic speaker
% verification anti-spoofing: Constant Q cepstral coefficients. ODYSSEY
% 2016, The Speaker and Language Recognition Workshop, June 21-24, 2016,
% Bilbao, Spain.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear
addpath('emovo');
addpath('CQT_toolbox_2013');

% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
%
% Input parameters for extration features:
%         x        : input signal
%         fs       : sampling frequency
%         B        : number of bins per octave [default = 96]
%         fmax     : highest frequency to be analyzed [default = Nyquist
%                    frequency]
%         fmin     : lowest frequency to be analyzed [default = ~20Hz
%                    to fullfill an integer number of octave]
%         d        : number of uniform samples in the first octave
%                    [default 16]
%         cf       : number of cepstral coefficients excluding 0'th
%                    coefficient [default 19]
%         ZsdD     : any sensible combination of the following
%                    [default ZsdD]:
%                      'Z'  include 0'th order cepstral coefficient
%                      's'  include static coefficients (c)
%                      'd'  include delta coefficients (dc/dt)
%                      'D'  include delta-delta coefficients (d^2c/dt^2)
%         f_d	    : delta window size [default = 2]
%         vad       : use voice activity detector [default = 0]
%         preE      : use pre-emphasis filter [default = 0]
%         arte      : use ARTE filtering [default = 0]
%         yw_cf     : order of Yule-Walker recursive filter
%                     (for ARTE filter) [default 3]
%         cmvn      : use Cepstral mean and variance norm filtering
%                     [default = 0]
%
% Output parameters:
%         CQcc              : constant Q cepstral coefficients
%                             (nCoeff x nFea)
%         LogP_absCQT       : log power magnitude spectrum of constant
%                             Q trasform
%         TimeVec           : time at the centre of each frame [sec]
%         FreqVec           : center frequencies of analysis filters [Hz]
%         Ures_LogP_absCQT  : uniform resampling of LogP_absCQT
%         Ures_FreqVec      : uniform resampling of FreqVec [Hz]
%         VAD               : voice activity decision [0 or 1]
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------

dataMFCC = [];
dataCQCC = [];

% input parameters for extract fetures
B = 96;
d = 16;
cf = 19;
ZsdD = 'ZsdD';
f_d = 2;
vad = 1;
preE = 1;
arte = 1;
yw_cf = 3; 
cmvn = 1;

WAVinfo = dir(['emovo/cqcc/' '' '/*.wav']);
namesWAV = {WAVinfo.name};

for i=1:length(namesWAV)
    fprintf( 'FILE: %s', char(namesWAV(i)) );
    [x, fs] = preprocessing( char(namesWAV(i)) );

    % input parameters again
    fmax = fs / 2;
    fmin = fmax / 2^9;

    fprintf('\n\nthe parameters for CQCC func:');
    fprintf('\nfs: %i, B: %i, d: %i, cf: %i, ZsdD: %s, f_d: %i,', ...
            fs, B, d, cf, ZsdD, f_d);
    fprintf('\nvad: %i, preE: %i, arte: %i, yw_cf: %i, cmvn: %i.', ...
            vad, preE, arte, yw_cf, cmvn);

    [CQcc, LogP_absCQT, TimeVec, FreqVec, Ures_LogP_absCQT, Ures_FreqVec, VAD] = ...
    cqcc(x, fs, B, fmax, fmin, d, cf, ZsdD, f_d, vad, preE, arte, yw_cf, cmvn);

    fprintf('\n\nCQCC feature extraction successfully completed.');
    fprintf('\nsize of CQCC matrix:');
    disp(size(CQcc));

    [emoVal, spkrVal, phrsVal] = getattr(char(namesWAV(i)), 'emovo/');
    fprintf('\nattributes have been extracted.');

    % formation of a matrix of features and attributes
	cqccFeat = getalldata(emoVal, spkrVal, phrsVal, CQcc);
    dataCQCC = cat(1, dataCQCC, cqccFeat);

end

fprintf('\n\ndata (matrix of MFCC&CQCC and attributes) have been extracted.');
fprintf('\n\nwrite the data to file. wait...');      
save('results/features.h5', 'dataMFCC', 'dataCQCC', '-v7.3');
fprintf('\n\nthe data successfully recorded.\nexit!\n');