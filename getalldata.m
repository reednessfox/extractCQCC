function [dataCQCC, dataMFCC] = getalldata(emo, spkr, phrs, MFCC, deltaMFCC, deltaDeltaMFCC, CQcc)

tmpEmoCQCC  = [];
tmpSpkrCQCC = [];
tmpPhrsCQCC = [];
tmpEmoMFCC  = [];
tmpSpkrMFCC = [];
tmpPhrsMFCC = [];
attrCQCC = [];
attrMFCC = [];

szCQCC = size(CQcc);
szMFCC = size(MFCC);

for k=1:szCQCC(2)
    tmpEmoCQCC  = [tmpEmoCQCC, emo];
    tmpSpkrCQCC = [tmpSpkrCQCC, spkr];
    tmpPhrsCQCC = [tmpPhrsCQCC, phrs];
end

attrCQCC = cat(2, attrCQCC, tmpEmoCQCC');
attrCQCC = cat(2, attrCQCC, tmpSpkrCQCC');
attrCQCC = cat(2, attrCQCC, tmpPhrsCQCC');

dataCQCC = cat(2, CQcc', attrCQCC);

fprintf('\nthe matrix dataCQCC is formed.');
disp(size(dataCQCC));

for k=1:szMFCC(1)
    tmpEmoMFCC  = [tmpEmoMFCC, emo];
    tmpSpkrMFCC = [tmpSpkrMFCC, spkr];
    tmpPhrsMFCC = [tmpPhrsMFCC, phrs];
end

attrMFCC = cat(1, attrMFCC, tmpEmoMFCC);
attrMFCC = cat(1, attrMFCC, tmpSpkrMFCC);
attrMFCC = cat(1, attrMFCC, tmpPhrsMFCC);
dataMFCC = cat(2, MFCC( : , 2:end), deltaMFCC( : , 2:end));
dataMFCC = cat(2, dataMFCC, deltaDeltaMFCC( : , 2:end));
dataMFCC = cat(2, dataMFCC, attrMFCC');

end